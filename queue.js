let collection = [];

function enqueue(element) {
    if (!Array.isArray(collection)) {
        collection = [];
    }

    var lastIndex = collection.length;
    collection[lastIndex] = element;
    return collection;
}

function dequeue() {
    if (collection.length === 0) {
        return "Queue is empty.";
    }
    collection.shift();
    return collection;
}


function front() {
    if (collection.length === 0) {
        return "Queue is empty.";
    }
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

function print() {
    return collection;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
};
